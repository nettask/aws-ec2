#!/bin/bash

. 01-var.sh
. 02-funcoes.sh

# 0 desligado, 1 ligado
executa=1

if [ "${executa}" -eq 1 ]; then
    doSeparador "Repositorio do NodeJs..."
    vs=12
    curl -sL https://deb.nodesource.com/setup_${vs}.x | bash -

    doSeparador "Instalando o NodeJs..."

    apps="nodejs"

    apt update && apt dist-upgrade -y && apt install -y ${apps} && apt -y autoremove
fi