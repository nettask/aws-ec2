#!/bin/bash
apt update
apt install -y git
txt="AWS-vault"
dns="testevault.runcap.com.br"
usr="admin"
cd "/home/${usr}"
mkdir projetos
cd projetos
git clone https://gitlab.com/nettask/aws-runcap.git
git clone https://github.com/certbot/certbot.git
cd aws-runcap
chmod +x *.sh
# --java
# --nodejs
# --ionic
# --nginx
# --vault
./00-run.sh "${txt}" "${dns}" "${usr}" --nginx --vault
chown "${usr}.${usr}" "/home/${usr}/projetos" -R